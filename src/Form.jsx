import React, {useState} from 'react';

export function Form({ value, onSubmit }) {
        const [ inputValues, setInputValues ] = useState(value || {})
      
        const handleInputChange = (event) => {
          setInputValues({
            ...inputValues,
            [event.target.name]: event.target.value
          })
        }
      
        const handleSubmit = (event) => {
          event.preventDefault();
          onSubmit(inputValues);
          setInputValues({});
        }
      
        return (
          <div className="form-style">
            <h2>Přidání/změna kosmonauta</h2>
            <form onSubmit={handleSubmit}>
              <label>
                Jméno:
                <input type="text" name="name" value={inputValues.name || ''} onChange={handleInputChange} autoComplete='off' />
              </label>
              <br />
              <label>
                Příjmení:
                <input type="text" name="surname" value={inputValues.surname || ''} onChange={handleInputChange} autoComplete='off' />
              </label>
              <br />
              <label>
                Datum narození:
                <input type="date" name="date" value={inputValues.date || ''} onChange={handleInputChange} autoComplete='off' />
              </label>
              <br />
              <label>
                Superchopnost:
                <input type="text" name="superpower" value={inputValues.superpower || ''} onChange={handleInputChange} autoComplete='off' />
              </label>
              <br />
              <button type="submit">Uložit</button>
            </form>
          </div>
        )
      }
