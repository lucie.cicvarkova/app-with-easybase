import React from "react";
import icon from './editicon.svg';

export function DefaultRow() {
    return (
        <tr>
            <td>Jurij</td>
            <td>Gagarin</td>
            <td>9.3. 1934</td>
            <td >První</td>
            <td><img src={icon} className="edit-icon" alt="edit icon" /></td>
            <td style={{ color: 'black' }}>X</td>
        </tr>
    )
}