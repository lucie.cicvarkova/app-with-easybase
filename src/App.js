import React from 'react';
import './App.css';
import {DatabaseListing } from './DatabaseListing';
import { EasybaseProvider } from "easybase-react";
import ebconfig from "./ebconfig";

function App() {
  return (
    <EasybaseProvider ebconfig={ebconfig}>
      <DatabaseListing />
    </EasybaseProvider>
  );
}

export default App;
