import React from "react";
import { useEasybase } from 'easybase-react';
import { useEffect } from 'react';
import { Header } from "./Header.jsx";
import icon from "./editicon.svg";

export function Table(props) {
  
  const { Frame, sync, configureFrame } = useEasybase();

  useEffect(() => {
    configureFrame({ tableName: "ASTRONAUTS", limit: 10 });
    sync();
  });

  return (
    <div className="table-style">
      <h1>SEZNAM KOSMONAUTŮ</h1>     
      <table>
        <Header />
        <tbody>
          {
            Frame().map((astronaut, index) => (              
              <tr key={index}>
                <td>{astronaut.name}</td>
                <td>{astronaut.surname}</td>
                <td>{new Date(astronaut.date).toLocaleDateString()}</td>
                <td>{astronaut.superpower}</td>
                <td><button onClick={() => props.onEditClick(index)}><img src={icon} className="edit-icon" alt="edit icon" /></button></td>
                <td><button onClick={() => props.onDeleteClick(index)}><p style={{ color: 'black' }}>X</p></button></td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  )
}
