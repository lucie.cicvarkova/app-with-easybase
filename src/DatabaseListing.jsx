import React, { useState } from 'react';
import { useEasybase } from 'easybase-react';
import { Table } from './Table.jsx';
import { Form } from "./Form.jsx";

export function DatabaseListing() {

  const { Frame, sync } = useEasybase();
  const [editedIndex, setEditedIndex] = useState(null);

  const handleFormSubmit = (astronaut) => {
    if (editedIndex === null) {
      Frame().push(astronaut);
      sync();
    } else {
      Frame()[editedIndex] = astronaut;
      sync();
      setEditedIndex(null);
    }
  }
 
  const onEditClick = (index) => {
    setEditedIndex(index);
  }

  const deleteItem = (index) => {
    Frame().splice(index, 1);
    sync();
    setEditedIndex(null);
  }

  return (
    <div className="wrapper grid">
      <Table onEditClick={onEditClick}
        onDeleteClick={deleteItem} />
      <Form key={editedIndex}
        value={editedIndex === null ? null : Frame(editedIndex)}
        onSubmit={handleFormSubmit} />
</div>
  );
}
